import copy
import uuid

import pytest


@pytest.mark.asyncio
async def test_secret(hub, ctx, version):
    if version == "v2":
        return
    secret_name = "idem-test-kv-v1-key-" + str(uuid.uuid4())
    path = "secret/idem-test-kv-v1-key"
    # create secrets with 2 keys in the above path
    data = {"my-secret": "my-secret-value", "new-secret": "new-secret-value"}
    ret = await hub.states.vault.secrets.kv_v1.secret.present(
        ctx=ctx, name=secret_name, path=path, data=data
    )
    assert ret["result"], ret["comment"]

    # Add one more secret to the same path using key state
    key = "secret-key"
    value = "secret-value"
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx=test_ctx, name=secret_name, path=path, key=key, value=value
    )
    assert ret["result"], ret["comment"]
    assert f"Would create vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")

    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx=ctx, name=secret_name, path=path, key=key, value=value
    )
    assert ret["result"], ret["comment"]
    assert f"Created vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")

    # Search using secret state should return newly added key along with other keys
    ret = await hub.states.vault.secrets.kv_v1.secret.search(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert 3 == len(resource.get("data"))

    # Update a key with new value with --test flag
    value = "update-secret-value"
    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx=test_ctx, name=secret_name, path=path, key=key, value=value
    )
    assert ret["result"], ret["comment"]
    assert f"Would update vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")

    ret = await hub.states.vault.secrets.kv_v1.key.present(
        ctx=ctx, name=secret_name, path=path, key=key, value=value
    )
    assert ret["result"], ret["comment"]
    assert f"Updated vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert key == resource.get("key")
    assert value == resource.get("value")

    # delete the key with --test
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        ctx=test_ctx, name=secret_name, path=path, key=key
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Delete key in real
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        ctx=ctx, name=secret_name, path=path, key=key
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted vault.secrets.kv_v1.key '{key}'." in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Deleted vault.secrets.kv_v1.key '{key}'." in ret["comment"]

    # Delete the same key again
    ret = await hub.states.vault.secrets.kv_v1.key.absent(
        ctx=ctx, name=secret_name, path=path, key=key
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]
    assert f"vault.secrets.kv_v1.key '{key}' is already absent." in ret["comment"]

    # Search should now return only 2 keys
    ret = await hub.states.vault.secrets.kv_v1.secret.search(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert 2 == len(resource.get("data"))

    # Destroy all keys in the path
    # Delete secret with all versions with test
    ret = await hub.states.vault.secrets.kv_v1.secret.absent(
        ctx=test_ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
