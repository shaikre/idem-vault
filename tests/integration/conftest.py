import asyncio
from typing import List
from unittest import mock

import dict_tools.data
import nest_asyncio
import pop.hub
import pytest


@pytest.fixture(scope="module")
def acct_subs() -> List[str]:
    return ["vault"]


@pytest.fixture(
    name="version", scope="module", params=["v1", "v2"], ids=["kv_v1", "kv_v2"]
)
def kv_version(request):
    yield request.param


@pytest.fixture(scope="module")
def acct_profile(version: str) -> str:
    # The name of the profile that will be used for tests
    # Do NOT use production credentials for tests!
    # A profile with this name needs to be defined in the $ACCT_FILE in order to run these tests
    return f"test_development_idem_vault_{version}"


@pytest.fixture(scope="module")
def acct_data(ctx, version):
    """
    acct_data that can be used in running simple yaml blocks
    """
    yield {"profiles": {"vault": {"default": ctx.acct}}}


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    nest_asyncio.apply(loop=loop)
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(hub, acct_subs, acct_profile, version, tests_dir):
    ctx = dict_tools.data.NamespaceDict(
        run_name="test", tag="fake_|-test_|-tag", test=False
    )

    # Use the default unencrypted credentials file if none was found in the environment
    acct_file = (
        hub.OPT.acct.acct_file or tests_dir.parent / "example" / "credentials.yml"
    )
    await hub.acct.init.unlock(acct_file, hub.OPT.acct.acct_key)
    ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)

    hub.log.debug(
        f"Set VAULT_ADDR to '{ctx.acct['address']} 'with VAULT_TOKEN '{ctx.acct['token']}', version: {ctx.acct['version']}"
    )
    yield ctx


@pytest.fixture(scope="module", name="hub")
def integration_hub(event_loop):
    with mock.patch("sys.argv", ["idem_vault"]):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        for dyne in ["idem"]:
            hub.pop.sub.add(dyne_name=dyne)
        hub.pop.config.load(["idem", "acct"], cli="idem", parse_cli=False)

    yield hub
